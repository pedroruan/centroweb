package br.com.centroteresadavila.usuario.controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import br.com.centroteresadavila.dto.UsuarioDTO;
import br.com.centroteresadavila.repository.UsuarioRepository;
import br.com.centroteresadavila.repository.entity.UsuarioEntity;
import br.com.centroteresadavila.service.impl.PacienteServiceImpl;
import br.com.centroteresadavila.uteis.Uteis;

@Named(value="usuarioController")
@SessionScoped
public class UsuarioController implements Serializable {
 
	private static final long serialVersionUID = 1L;
	
	private Logger LOGGER = Logger.getLogger(UsuarioController.class);
 
	@Inject
	private UsuarioDTO usuarioDTO;
 
	@Inject
	private UsuarioRepository usuarioRepository;
 
	@Inject
	private UsuarioEntity usuarioEntity;
 
	public UsuarioDTO getUsuarioDTO() {
		return usuarioDTO;
	}
	public void setUsuarioDTO(UsuarioDTO usuarioModel) {
		this.usuarioDTO = usuarioModel;
	}
 
	public UsuarioDTO GetUsuarioSession(){
 
		FacesContext facesContext = FacesContext.getCurrentInstance();
 
		return (UsuarioDTO)facesContext.getExternalContext().getSessionMap().get("usuarioAutenticado");
	}
 
	public String Logout(){
 
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
 
		return "/sistema/index.xhtml?faces-redirect=true";
	}
	public String EfetuarLogin(){
 
		if(StringUtils.isEmpty(usuarioDTO.getUsuario()) || StringUtils.isBlank(usuarioDTO.getUsuario())){
 
			Uteis.Mensagem("Favor informar o login!");
			return null;
		}
		else if(StringUtils.isEmpty(usuarioDTO.getSenha()) || StringUtils.isBlank(usuarioDTO.getSenha())){
 
			Uteis.Mensagem("Favor informara senha!");
			return null;
		}
		else{	
 
			usuarioEntity = usuarioRepository.ValidaUsuario(usuarioDTO);
 
			if(usuarioEntity!= null){
 
				usuarioDTO.setSenha(null);
				usuarioDTO.setCodigo(usuarioEntity.getCodigo());
 
 
				FacesContext facesContext = FacesContext.getCurrentInstance();
 
				facesContext.getExternalContext().getSessionMap().put("usuarioAutenticado", usuarioDTO);
 
				
				return "/sistema/home.xhtml?faces-redirect=true";
				//return "sistema/home.xhtml?faces-redirect=true";
			}
			else{
 
				Uteis.Mensagem("N�o foi poss�vel efetuar o login com esse usu�rio e senha!");
				return null;
			}
		}
 
 
	}
}
