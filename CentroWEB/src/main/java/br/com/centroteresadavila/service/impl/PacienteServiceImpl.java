package br.com.centroteresadavila.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import br.com.centroteresadavila.dao.PacienteDAO;
import br.com.centroteresadavila.dto.PacienteDTO;
import br.com.centroteresadavila.repository.entity.PacienteEntity;
import br.com.centroteresadavila.service.PacienteService;

public class PacienteServiceImpl implements PacienteService {
	
	private Logger LOGGER = Logger.getLogger(PacienteServiceImpl.class);
	
	private static final String INATIVO = "Inativo";
	
	@Inject
	private PacienteDAO pacienteDAO;
	
	@Inject
	private PacienteEntity paciente;
	
	@Override
	public Boolean cadastrar(PacienteDTO dto) {
		
		try {
			
			paciente  = new PacienteEntity();
			
			paciente.setNome(dto.getNome());
			paciente.setRg(dto.getRg());
			paciente.setEndereco(dto.getEndereco());
			paciente.setBairro(dto.getBairro());
			paciente.setNumero(dto.getNumero());
			paciente.setCep(dto.getCep());
			paciente.setCidade(dto.getCidade());
			paciente.setGenero(dto.getGenero());
			paciente.setUf(dto.getUf());
			paciente.setObservacao(dto.getObservacao()!=null ? dto.getObservacao(): "");
			paciente.setFone(dto.getFone());
			paciente.setNascimento(dto.getNascimento());
			paciente.setTratamentoAtivo(INATIVO);
			
			return pacienteDAO.cadastrar(paciente);
			
		} catch (Exception e) {
			LOGGER.error(e);
			
		}
		
		return false;
		
	}

	@Override
	public List<PacienteDTO> pesquisar(PacienteDTO dto) {
		List<PacienteDTO> lista = new ArrayList<PacienteDTO>();
		List<PacienteEntity> listaRetorno = new ArrayList<PacienteEntity>(); 
		
		PacienteDTO item = new PacienteDTO();
		
		if(!Objects.isNull(dto.getNome())) {
			paciente.setNome(dto.getNome());
			listaRetorno = pacienteDAO.pesquisarPorNome(paciente);
		}else{
			listaRetorno = pacienteDAO.pesquisarTodos(paciente);
		}
		
		for (int i = 0; i < listaRetorno.size(); i++) {
			item = new PacienteDTO();
			item.setNome(listaRetorno.get(i).getNome());
			item.setId(listaRetorno.get(i).getId());
			item.setEndereco(listaRetorno.get(i).getEndereco());
			item.setBairro(listaRetorno.get(i).getBairro());
			item.setNumero(listaRetorno.get(i).getNumero());
			item.setNascimento(listaRetorno.get(i).getNascimento());
			item.setCidade(listaRetorno.get(i).getCidade());
			item.setObservacao(listaRetorno.get(i).getObservacao());
			item.setTratamentoAtivo(listaRetorno.get(i).getTratamentoAtivo());
			item.setFone(listaRetorno.get(i).getFone());
			item.setUf(listaRetorno.get(i).getUf());
			lista.add(item);
		}
		
		return lista;
	}

}
