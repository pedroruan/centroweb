package br.com.centroteresadavila.service;

import java.util.List;

import br.com.centroteresadavila.dto.PacienteDTO;

public interface PacienteService {

	public Boolean cadastrar(PacienteDTO dto);
	
	public List<PacienteDTO> pesquisar(PacienteDTO dto);
	
	
}
