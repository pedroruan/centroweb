package br.com.centroteresadavila.service;

import java.util.List;

import br.com.centroteresadavila.dto.PacienteDTO;
import br.com.centroteresadavila.dto.TratamentoDTO;

public interface TratamentoService {

	public Boolean cadastrar(TratamentoDTO dto);
	
	public List<TratamentoDTO> pesquisar(TratamentoDTO dto);
	
	
}
