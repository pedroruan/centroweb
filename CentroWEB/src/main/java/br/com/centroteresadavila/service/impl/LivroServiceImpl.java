package br.com.centroteresadavila.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import br.com.centroteresadavila.dao.EmprestimoDAO;
import br.com.centroteresadavila.dao.LivroDAO;
import br.com.centroteresadavila.dao.PacienteDAO;
import br.com.centroteresadavila.dto.EmprestimoDTO;
import br.com.centroteresadavila.dto.LivroDTO;
import br.com.centroteresadavila.repository.entity.EmprestimoEntity;
import br.com.centroteresadavila.repository.entity.LivroEntity;
import br.com.centroteresadavila.repository.entity.PacienteEntity;
import br.com.centroteresadavila.service.LivroService;

public class LivroServiceImpl implements LivroService {
	
	private Logger LOGGER = Logger.getLogger(LivroServiceImpl.class);
	
	@Inject
	private LivroDAO livroDAO;
	
	@Inject
	private EmprestimoDAO emprestimoDAO;
	
	@Inject
	private PacienteDAO pacienteDAO;
	
	@Inject
	private LivroEntity livro;
	
	@Override
	public Boolean cadastrar(LivroDTO dto) {
		
		try {
			DateFormat dataFormatada = new SimpleDateFormat("dd/MM/yyyy"); 
			String dataString = null;
			
			Date data = new Date();
			dataString = dataFormatada.format(data);
			
			livro  = new LivroEntity();
			
			livro.setNome(dto.getNome());
			livro.setAutor(dto.getAutor());
			livro.setEstado(dto.getEstado());
			livro.setObservacao(dto.getObservacao());
			livro.setStatus("Disponivel");
			livro.setDataCadastro(dataFormatada.parse(dataString));
			
			return livroDAO.cadastrar(livro);
			
		} catch (Exception e) {
			LOGGER.error(e);
			
		}
		
		return false;
		
	}

	@Override
	public List<LivroDTO> pesquisar(LivroDTO dto) {
		List<LivroDTO> lista = new ArrayList<LivroDTO>();
		List<LivroEntity> listaRetorno = new ArrayList<LivroEntity>(); 
		
		LivroDTO item = new LivroDTO();
		
		if(!Objects.isNull(dto.getNome()) || !Objects.isNull(dto.getAutor()) || !Objects.isNull(dto.getStatus())) {
		
			livro.setNome(dto.getNome().equals("") ? "" : dto.getNome());
			
			livro.setAutor(dto.getAutor().equals("") ? "" : dto.getAutor());
			
			livro.setStatus(dto.getStatus().equals("") ? "" :  dto.getStatus());
			
			listaRetorno = livroDAO.pesquisarPorNome(livro);
			
		}else{
			
			listaRetorno = livroDAO.pesquisarTodos(livro);
			
		}
		
		for (int i = 0; i < listaRetorno.size(); i++) {
			item = new LivroDTO();
			item.setNome(listaRetorno.get(i).getNome());
			item.setId(listaRetorno.get(i).getId());
			item.setAutor(listaRetorno.get(i).getAutor());
			item.setDataCadastro(listaRetorno.get(i).getDataCadastro().toString());
			item.setEstado(listaRetorno.get(i).getEstado());
			item.setObservacao(listaRetorno.get(i).getObservacao());
			item.setStatus(listaRetorno.get(i).getStatus());
			
			lista.add(item);
		}
		
		return lista;
	}

	@Override
	public List<EmprestimoDTO> pesquisarEmprestimos(LivroDTO dto) {
	
		
		
		
		
		return null;
	}
	
	
	@Override
	public LivroDTO detalharEmprestimo(LivroDTO dto) {
	
		LivroDTO retorno = new LivroDTO();
		EmprestimoDTO emprestimo = new EmprestimoDTO();
		EmprestimoEntity entrada = new EmprestimoEntity();
		EmprestimoEntity lista = new EmprestimoEntity();
		PacienteEntity paciente = new PacienteEntity();
		
		retorno = dto;
		retorno.setEmprestimo(emprestimo);
		
		entrada.setIdlivro(dto.getId());
		
		lista = emprestimoDAO.detalharEmprestimo(entrada);
		
		if(!Objects.isNull(retorno)) {
			
			paciente = pacienteDAO.pesquisarPorId(lista.getIdpaciente());
			retorno.getEmprestimo().setIdpaciente(lista.getIdpaciente());
			retorno.getEmprestimo().setNomePaciente(paciente.getNome());
			retorno.getEmprestimo().setId(lista.getId());
			retorno.getEmprestimo().setDataEmprestimo(lista.getDataemprestimo().toString());
		}
		
		return retorno;
	}

}
