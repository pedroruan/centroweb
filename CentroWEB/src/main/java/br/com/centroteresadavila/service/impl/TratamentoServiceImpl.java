package br.com.centroteresadavila.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import br.com.centroteresadavila.dao.PacienteDAO;
import br.com.centroteresadavila.dao.PassesDAO;
import br.com.centroteresadavila.dao.TratamentoDAO;
import br.com.centroteresadavila.dto.PacienteDTO;
import br.com.centroteresadavila.dto.PassesDTO;
import br.com.centroteresadavila.dto.TratamentoDTO;
import br.com.centroteresadavila.repository.entity.PacienteEntity;
import br.com.centroteresadavila.repository.entity.PassesEntity;
import br.com.centroteresadavila.repository.entity.TratamentoEntity;
import br.com.centroteresadavila.service.TratamentoService;

public class TratamentoServiceImpl implements TratamentoService {

	private Logger LOGGER = Logger.getLogger(TratamentoServiceImpl.class);

	@Inject
	private PacienteDAO pacienteDAO;

	@Inject
	private PacienteEntity paciente;

	@Inject
	private TratamentoDAO tratamentoDAO;

	@Inject
	private TratamentoEntity tratamento;

	@Inject
	private PassesDAO passesDAO;

	@Inject
	private PassesEntity passes;

	@Override
	public Boolean cadastrar(TratamentoDTO dto) {

		try {

			Integer codigoTratamento = cadastrarPasses(dto.getPassesTratamento());
				
			

			tratamento = new TratamentoEntity();

			tratamento.setId_paciente(dto.getIdPaciente());
			tratamento.setIdpasse(codigoTratamento);

			String data = new SimpleDateFormat("dd/MM/yyyy").format(new Date());

			tratamento.setDtentrevista(data);


		} catch (Exception e) {
			LOGGER.error(e);

		}

		return false;

	}

	public Integer cadastrarPasses(List<PassesDTO> passesDto) {

		Integer codigoIdPasse = passesDAO.recuperarValorMaximo();
		
		for (int i = 0; i < passesDto.size(); i++) {

			passes = new PassesEntity();
			
			String data = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
			passes.setId(codigoIdPasse);
			passes.setDataMarcada(data);
			passes.setSequencialPasse(i+1);
			passes.setTipoPasse(passesDto.get(i).getTp_passe());
			passesDAO.cadastrar(passes);

		}
		
		return codigoIdPasse;

	}

	@Override
	public List<PacienteDTO> pesquisar(PacienteDTO dto) {
		List<PacienteDTO> lista = new ArrayList<PacienteDTO>();
		List<PacienteEntity> listaRetorno = new ArrayList<PacienteEntity>();

		PacienteDTO item = new PacienteDTO();

		if (!Objects.isNull(dto.getNome())) {
			paciente.setNome(dto.getNome());
			listaRetorno = pacienteDAO.pesquisarPorNome(paciente);
		} else {
			listaRetorno = pacienteDAO.pesquisarTodos(paciente);
		}

		for (int i = 0; i < listaRetorno.size(); i++) {
			item = new PacienteDTO();
			item.setNome(listaRetorno.get(i).getNome());
			item.setId(listaRetorno.get(i).getId());
			item.setEndereco(listaRetorno.get(i).getEndereco());
			item.setBairro(listaRetorno.get(i).getBairro());
			item.setNumero(listaRetorno.get(i).getNumero());
			item.setNascimento(listaRetorno.get(i).getNascimento());
			item.setCidade(listaRetorno.get(i).getCidade());
			item.setObservacao(listaRetorno.get(i).getObservacao());
			item.setTratamentoAtivo(listaRetorno.get(i).getTratamentoAtivo());
			item.setFone(listaRetorno.get(i).getFone());
			lista.add(item);
		}

		return lista;
	}

}
