package br.com.centroteresadavila.service;

import java.util.List;

import br.com.centroteresadavila.dto.EmprestimoDTO;
import br.com.centroteresadavila.dto.LivroDTO;

public interface LivroService {

	public Boolean cadastrar(LivroDTO dto);
	
	public List<LivroDTO> pesquisar(LivroDTO dto);
	
	public List<EmprestimoDTO> pesquisarEmprestimos(LivroDTO dto);
	
	public LivroDTO detalharEmprestimo(LivroDTO dto);
	
}
