package br.com.centroteresadavila.dao;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import br.com.centroteresadavila.repository.entity.PacienteEntity;
import br.com.centroteresadavila.repository.entity.PassesEntity;
import br.com.centroteresadavila.uteis.Uteis;

@RequestScoped
public class PassesDAO {
	
	private Logger LOGGER = Logger.getLogger(PassesDAO.class);
	
	private EntityManager entity = Uteis.JpaEntityManager();
	
	public Boolean cadastrar(PassesEntity dto) {
		
		try {
			
			entity.persist(dto);
			
			return true;
			
			
		} catch (Exception e) {
			LOGGER.error(e);
			return false;
		}
	}
	
	
	public Integer recuperarValorMaximo() {
		
		Integer count = 0;
		
		return count;
		
	}
	
}
