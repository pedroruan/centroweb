package br.com.centroteresadavila.dao;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import br.com.centroteresadavila.repository.entity.PacienteEntity;
import br.com.centroteresadavila.uteis.Uteis;

@RequestScoped
public class TratamentoDAO {
	
	private Logger LOGGER = Logger.getLogger(TratamentoDAO.class);
	
	private EntityManager entity = Uteis.JpaEntityManager();
	
	public Boolean cadastrar(PacienteEntity paciente) {
		
		try {
			
			entity.persist(paciente);
			
			return true;
			
			
		} catch (Exception e) {
			LOGGER.error(e);
			return false;
		}
	}
	
	public List<PacienteEntity> pesquisarPorNome(PacienteEntity paciente) {
		
		List<PacienteEntity> retorno = new ArrayList<PacienteEntity>();
		
		try {
			retorno =  entity.createQuery("select r from PacienteEntity r where r.nome like:nome").
						setParameter("nome", paciente.getNome() + "%").getResultList();
		} catch (Exception e) {
			LOGGER.error(e);
		}
		
		return retorno;
		
	}
	
	public List<PacienteEntity> pesquisarTodos(PacienteEntity paciente) {
		
		try {
	
				return entity.createQuery("select r from PacienteEntity r").getResultList();
			
		} catch (Exception e) {
			LOGGER.error(e);
		}
		
		return null;
		
	}
	
	public PacienteEntity pesquisarPorId(String id) {
		
		PacienteEntity retorno = new PacienteEntity();
		
		try {
			retorno = (PacienteEntity) entity.createQuery("select r from PacienteEntity r where r.id = :id").
						setParameter("id", id).getSingleResult();
		} catch (Exception e) {
			LOGGER.error(e);
		}
		
		return retorno;
		
	}
	
}
