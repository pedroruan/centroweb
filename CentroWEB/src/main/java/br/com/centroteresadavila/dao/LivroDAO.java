package br.com.centroteresadavila.dao;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import br.com.centroteresadavila.repository.entity.EmprestimoEntity;
import br.com.centroteresadavila.repository.entity.LivroEntity;
import br.com.centroteresadavila.uteis.Uteis;

@RequestScoped
public class LivroDAO {
	
	private Logger LOGGER = Logger.getLogger(LivroDAO.class);
	
	private EntityManager entity = Uteis.JpaEntityManager();
	
	public Boolean cadastrar(LivroEntity dto) {
		
		try {
			
			entity.persist(dto);
			
			return true;
			
			
		} catch (Exception e) {
			LOGGER.error(e);
			return false;
		}
	}
	
	public List<LivroEntity> pesquisarPorNome(LivroEntity dto) {
		
		List<LivroEntity> retorno = new ArrayList<LivroEntity>();
		
		try {
			retorno =  entity.createQuery("select r from LivroEntity r where r.nome like:nome and r.autor like:autor and r.status like:status").
						setParameter( "nome", "%" + dto.getNome() + "%").
						setParameter("autor", "%" + dto.getAutor() + "%").
						setParameter("status", "%" + dto.getStatus() + "%").
						getResultList();
		} catch (Exception e) {
			LOGGER.error(e);
		}
		
		return retorno;
		
	}
	
	public List<LivroEntity> pesquisarTodos(LivroEntity dto) {
		
		try {
	
				return entity.createQuery("select r from LivroEntity r").getResultList();
			
		} catch (Exception e) {
			LOGGER.error(e);
		}
		
		return null;
		
	}
	
	public List<EmprestimoEntity> pesquisarEmprestimos(EmprestimoEntity dto){
		
		try {
			
			return entity.createQuery("select r from EmprestimoEntity r").getResultList();
			
		} catch (Exception e) {
			LOGGER.error(e);
		}
		
		return null;
		
	}
	
	public EmprestimoEntity detalharEmprestimo(EmprestimoEntity dto){
		
		try {
			
			 entity.createQuery("select r from EmprestimoEntity r where r.idlivro : idlivro ").
					setParameter( "idlivro",  dto.getIdlivro()).getResultList();
			
		} catch (Exception e) {
			LOGGER.error(e);
		}
		
		return null;
		
	}
	
	
}
