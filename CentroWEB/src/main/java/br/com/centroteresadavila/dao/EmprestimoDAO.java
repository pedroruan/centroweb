package br.com.centroteresadavila.dao;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import br.com.centroteresadavila.repository.entity.EmprestimoEntity;
import br.com.centroteresadavila.repository.entity.LivroEntity;
import br.com.centroteresadavila.uteis.Uteis;

@RequestScoped
public class EmprestimoDAO {
	
	private Logger LOGGER = Logger.getLogger(EmprestimoDAO.class);
	
	private EntityManager entity = Uteis.JpaEntityManager();
	
	public Boolean cadastrar(LivroEntity dto) {
		
		try {
			
			entity.persist(dto);
			
			return true;
			
			
		} catch (Exception e) {
			LOGGER.error(e);
			return false;
		}
	}
	
	public List<EmprestimoEntity> pesquisarPorNome(EmprestimoEntity dto) {
		
		return null;
		
	}

	
	public List<EmprestimoEntity> pesquisarTodos(EmprestimoEntity dto){
		
		try {
			
			return entity.createQuery("select r from EmprestimoEntity r").getResultList();
			
		} catch (Exception e) {
			LOGGER.error(e);
		}
		
		return null;
		
	}
	
	public EmprestimoEntity detalharEmprestimo(EmprestimoEntity dto){
		
		List<EmprestimoEntity> retorno = new ArrayList<EmprestimoEntity>();
		
		try {
			
			retorno = entity.createQuery("select r from EmprestimoEntity r").getResultList();
			
			if(retorno.size()>0) {
				return retorno.get(0);
			}
			
			
		} catch (Exception e) {
			LOGGER.error(e);
		}
		
		return null;
		
	}
	
	
}
