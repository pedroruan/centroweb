package br.com.centroteresadavila.dto;

import java.io.Serializable;
import java.util.List;

public class TratamentoDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id;
	private String idPaciente;
	private String dataEntrevista;
	private String observacao;
	private List<PassesDTO> passesTratamento;
	private String nome;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdPaciente() {
		return idPaciente;
	}
	public void setIdPaciente(String idPaciente) {
		this.idPaciente = idPaciente;
	}
	public String getDataEntrevista() {
		return dataEntrevista;
	}
	public void setDataEntrevista(String dataEntrevista) {
		this.dataEntrevista = dataEntrevista;
	}

	public List<PassesDTO> getPassesTratamento() {
		return passesTratamento;
	}
	public void setPassesTratamento(List<PassesDTO> passesTratamento) {
		this.passesTratamento = passesTratamento;
	}

	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
