package br.com.centroteresadavila.dto;

import java.io.Serializable;

public class PassesTratamento implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String id;
	private String idPaciente;
	private String idTratamento;
	private String tipoPasse;
	private String dataPasse;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdPaciente() {
		return idPaciente;
	}
	public void setIdPaciente(String idPaciente) {
		this.idPaciente = idPaciente;
	}
	public String getIdTratamento() {
		return idTratamento;
	}
	public void setIdTratamento(String idTratamento) {
		this.idTratamento = idTratamento;
	}
	public String getTipoPasse() {
		return tipoPasse;
	}
	public void setTipoPasse(String tipoPasse) {
		this.tipoPasse = tipoPasse;
	}
	public String getDataPasse() {
		return dataPasse;
	}
	public void setDataPasse(String dataPasse) {
		this.dataPasse = dataPasse;
	}
	
	
}
