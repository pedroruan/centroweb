package br.com.centroteresadavila.dto;

import java.io.Serializable;

public class EmprestimoDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String id;
	private String idpaciente;
	private String nomePaciente;
	private String idlivro;
	private String dataEmprestimo;
	private String status;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdpaciente() {
		return idpaciente;
	}
	public void setIdpaciente(String idpaciente) {
		this.idpaciente = idpaciente;
	}
	public String getIdlivro() {
		return idlivro;
	}
	public void setIdlivro(String idlivro) {
		this.idlivro = idlivro;
	}
	public String getDataEmprestimo() {
		return dataEmprestimo;
	}
	public void setDataEmprestimo(String dataEmprestimo) {
		this.dataEmprestimo = dataEmprestimo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getNomePaciente() {
		return nomePaciente;
	}
	public void setNomePaciente(String nomePaciente) {
		this.nomePaciente = nomePaciente;
	}
	

}
