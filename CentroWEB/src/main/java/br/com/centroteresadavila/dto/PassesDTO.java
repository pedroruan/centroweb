package br.com.centroteresadavila.dto;

import java.io.Serializable;

public class PassesDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String id;
	private String sequencial_passe;
	private String tp_passe;
	private String data_marcada;
	private String data_efetivada;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSequencial_passe() {
		return sequencial_passe;
	}
	public void setSequencial_passe(String sequencial_passe) {
		this.sequencial_passe = sequencial_passe;
	}
	public String getTp_passe() {
		return tp_passe;
	}
	public void setTp_passe(String tp_passe) {
		this.tp_passe = tp_passe;
	}
	public String getData_marcada() {
		return data_marcada;
	}
	public void setData_marcada(String data_marcada) {
		this.data_marcada = data_marcada;
	}
	public String getData_efetivada() {
		return data_efetivada;
	}
	public void setData_efetivada(String data_efetivada) {
		this.data_efetivada = data_efetivada;
	}
	
}
