package br.com.centroteresadavila.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import br.com.centroteresadavila.dto.UsuarioDTO;
import br.com.centroteresadavila.usuario.controller.UsuarioController;

/**
 * Servlet Filter implementation class AutenticacaoFilter
 */
@WebFilter("/sistema/*")
public class AutenticacaoFilter implements Filter {
 
	private Logger LOGGER = Logger.getLogger(AutenticacaoFilter.class);
	
    public AutenticacaoFilter() {
 
    }
 
	public void destroy() {
 
	}
 
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
 
		HttpSession httpSession 				= ((HttpServletRequest) request).getSession(); 
 
		HttpServletRequest httpServletRequest   = (HttpServletRequest) request;
 
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
 
		if(httpServletRequest.getRequestURI().indexOf("index.xhtml") <= -1){
 
			UsuarioDTO usuarioDTO =(UsuarioDTO) httpSession.getAttribute("usuarioAutenticado");
 
			if(usuarioDTO == null){
 
				httpServletResponse.sendRedirect(httpServletRequest.getContextPath()+ "/sistema/index.xhtml");
 
			}
			else{
 
				chain.doFilter(request, response);
			}
		}		
		else{
 
			chain.doFilter(request, response);
		}
	}
 
	public void init(FilterConfig fConfig) throws ServletException {
 
	}
 

}
