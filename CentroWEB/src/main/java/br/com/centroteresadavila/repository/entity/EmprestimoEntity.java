package br.com.centroteresadavila.repository.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="tb_emprestimos")
public class EmprestimoEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name = "idemprestimo", nullable = false)
	private String id;
	
	@Column(name = "idpaciente", nullable = false)
	private String idpaciente;
	
	@Column(name = "idlivro", nullable = false)
	private String idlivro;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "dataemprestimo")
	private Date dataemprestimo;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdpaciente() {
		return idpaciente;
	}

	public void setIdpaciente(String idpaciente) {
		this.idpaciente = idpaciente;
	}

	public String getIdlivro() {
		return idlivro;
	}

	public void setIdlivro(String idlivro) {
		this.idlivro = idlivro;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDataemprestimo() {
		return dataemprestimo;
	}

	public void setDataemprestimo(Date dataemprestimo) {
		this.dataemprestimo = dataemprestimo;
	}
	
}
