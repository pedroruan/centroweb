package br.com.centroteresadavila.repository.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.log4j.Logger;

import br.com.centroteresadavila.filters.JPAFilter;


@Entity
@Table(name="tb_tratamentos")
public class TratamentoEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4457509233554313342L;
	
	@Id
	@GeneratedValue
	@Column(name = "id_tratamento", nullable = false)
	private String id;
	
	@Column(name = "id_paciente")
	private String id_paciente;
	
	@Column(name = "dtentrevista", nullable = false)
	private String dtentrevista;
	
	@Column(name = "idpasse")
	private Integer idpasse;
	
	@Column(name = "observacao")
	private String observacao;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId_paciente() {
		return id_paciente;
	}

	public void setId_paciente(String id_paciente) {
		this.id_paciente = id_paciente;
	}

	public String getDtentrevista() {
		return dtentrevista;
	}

	public void setDtentrevista(String dtentrevista) {
		this.dtentrevista = dtentrevista;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Integer getIdpasse() {
		return idpasse;
	}

	public void setIdpasse(Integer idpasse) {
		this.idpasse = idpasse;
	}
	
	
	

}
