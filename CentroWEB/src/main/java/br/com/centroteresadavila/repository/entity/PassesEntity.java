package br.com.centroteresadavila.repository.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.log4j.Logger;

import br.com.centroteresadavila.filters.JPAFilter;


@Entity
@Table(name="tb_passes")
public class PassesEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4457509233554313342L;
	
	@Id
	@GeneratedValue
	@Column(name = "idpasses", nullable = false)
	private Integer id;
	
	@Column(name = "sequencial_passe", nullable = false)
	private Integer sequencialPasse;
	
	@Column(name = "tp_passe", nullable = false)
	private String tipoPasse;
	
	@Column(name = "data_marcada")
	private String dataMarcada;
	
	@Column(name = "data_efetivada")
	private String dataEfetivada;

	public String getTipoPasse() {
		return tipoPasse;
	}

	public void setTipoPasse(String tipoPasse) {
		this.tipoPasse = tipoPasse;
	}

	public String getDataMarcada() {
		return dataMarcada;
	}

	public void setDataMarcada(String dataMarcada) {
		this.dataMarcada = dataMarcada;
	}

	public String getDataEfetivada() {
		return dataEfetivada;
	}

	public void setDataEfetivada(String dataEfetivada) {
		this.dataEfetivada = dataEfetivada;
	}

	public Integer getSequencialPasse() {
		return sequencialPasse;
	}

	public void setSequencialPasse(Integer sequencialPasse) {
		this.sequencialPasse = sequencialPasse;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
}
