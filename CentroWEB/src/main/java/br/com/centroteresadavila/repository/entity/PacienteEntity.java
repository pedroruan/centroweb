package br.com.centroteresadavila.repository.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.log4j.Logger;

import br.com.centroteresadavila.filters.JPAFilter;


@Entity
@Table(name="tb_pacientes")
public class PacienteEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4457509233554313342L;
	
	@Id
	@GeneratedValue
	@Column(name = "idpaciente", nullable = false)
	private String id;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "rg", nullable = false)
	private String rg;
	
	@Column(name = "endereco")
	private String endereco;
	
	@Column(name = "numero")
	private String numero;
	
	@Column(name = "bairro")
	private String bairro;
	
	@Column(name = "cidade")
	private String cidade;
	
	@Column(name = "UF")
	private String uf;
	
	@Column(name = "nascimento")
	private String nascimento;
	
	@Column(name = "observacao")
	private String observacao;
	
	@Column(name = "genero")
	private String genero;
	
	@Column(name = "cep")
	private String cep;
	
	@Column(name = "fone")
	private String fone;

	@Column(name = "trat_ativo")
	private String tratamentoAtivo;
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getNascimento() {
		return nascimento;
	}

	public void setNascimento(String nascimento) {
		this.nascimento = nascimento;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getFone() {
		return fone;
	}

	public void setFone(String fone) {
		this.fone = fone;
	}

	public String getTratamentoAtivo() {
		return tratamentoAtivo;
	}

	public void setTratamentoAtivo(String tratamentoAtivo) {
		this.tratamentoAtivo = tratamentoAtivo;
	}
	
	
	
	

}
