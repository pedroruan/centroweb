package br.com.centroteresadavila.repository;

import java.io.Serializable;

import javax.persistence.Query;

import org.apache.log4j.Logger;

import br.com.centroteresadavila.dto.UsuarioDTO;
import br.com.centroteresadavila.filters.JPAFilter;
import br.com.centroteresadavila.repository.entity.UsuarioEntity;
import br.com.centroteresadavila.uteis.Uteis;
 
 
public class UsuarioRepository implements Serializable {
 
	private Logger LOGGER = Logger.getLogger(UsuarioRepository.class);
 
	private static final long serialVersionUID = 1L;
 
	public UsuarioEntity ValidaUsuario(UsuarioDTO usuarioModel){
 
		
		try {
 
			//QUERY QUE VAI SER EXECUTADA (UsuarioEntity.findUser) 	
			Query query = Uteis.JpaEntityManager().createNamedQuery("UsuarioEntity.findUser");
 
			//PAR�METROS DA QUERY
			query.setParameter("usuario", usuarioModel.getUsuario());
			query.setParameter("senha", usuarioModel.getSenha());
 
			//RETORNA O USU�RIO SE FOR LOCALIZADO
			return (UsuarioEntity)query.getSingleResult();
 
		} catch (Exception e) {
			LOGGER.error(e);
			return null;
		}
 
	}
}