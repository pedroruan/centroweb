package br.com.centroteresadavila.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import br.com.centroteresadavila.dto.PacienteDTO;
import br.com.centroteresadavila.dto.TratamentoDTO;
import br.com.centroteresadavila.service.PacienteService;


@Named("cadastroTratamentoBean")
@RequestScoped
//@javax.faces.view.ViewScoped
public class CadastroTratamentoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Logger LOGGER = Logger.getLogger(CadastroTratamentoBean.class);
	
	@Inject
	private PacienteDTO pacienteDTO;
	
	@Inject
	private TratamentoDTO tratamentoDTO;
	
	@Inject
	private PacienteService pacienteService;
	
	private TratamentoDTO dtoConsulta;
	
	private PacienteDTO itemSelecionado = new PacienteDTO();
	
	//@Inject
	//private TratamentoService tratamentoService;
	
	private List<PacienteDTO> listaPacientes = null;
	
	private Boolean renderizarTabela = Boolean.FALSE;
//	
//	public CadastroTratamentoBean() {
//		dtoConsulta = new TratamentoDTO();
//		dtoConsulta.setPaciente(new PacienteDTO());
//		tratamentoDTO = new TratamentoDTO();
//		tratamentoDTO.setPaciente(new PacienteDTO());
//	}
	
	public void cadastrar() {
		
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage message;
		
		if(pacienteService.cadastrar(pacienteDTO)) {
			message = new FacesMessage("Paciente Cadastrado com Sucesso");
			message.setSeverity(FacesMessage.SEVERITY_INFO);
			context.addMessage(null, message);
		}else {
			
			message = new FacesMessage("Falha ao realizar cadastro");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, message);
		}
		
        pacienteDTO = new PacienteDTO();
        
		this.pacienteDTO = null;
		
	}
	
	public void limpar() {
		pacienteDTO = new PacienteDTO();
		
	}
	
	public List<PacienteDTO> pesquisar() {
		listaPacientes = new ArrayList<PacienteDTO>();
		
		pacienteDTO.setNome(tratamentoDTO.getNome());
		
		listaPacientes = pacienteService.pesquisar(pacienteDTO);
		
		if(listaPacientes.size()>0) {
			renderizarTabela = Boolean.TRUE;
		}
        
        return listaPacientes;
        
	}
	
	public void selecionarPaciente(PacienteDTO dto) {
		
		this.itemSelecionado = dto;
	
	}
	

	public void recuperarPaciente(){

		this.dtoConsulta = tratamentoDTO;
		
	}


	public PacienteDTO getPacienteDTO() {
		return pacienteDTO;
	}


	public void setPacienteDTO(PacienteDTO pacienteDTO) {
		this.pacienteDTO = pacienteDTO;
	}

	public TratamentoDTO getDtoConsulta() {
		return dtoConsulta;
	}

	public void setDtoConsulta(TratamentoDTO dtoConsulta) {
		this.dtoConsulta = dtoConsulta;
	}

	public List<PacienteDTO> getListaPacientes() {
		return listaPacientes;
	}

	public void setListaPacientes(List<PacienteDTO> listaPacientes) {
		this.listaPacientes = listaPacientes;
	}

	public TratamentoDTO getTratamentoDTO() {
		return tratamentoDTO;
	}

	public void setTratamentoDTO(TratamentoDTO tratamentoDTO) {
		this.tratamentoDTO = tratamentoDTO;
	}

	public PacienteDTO getItemSelecionado() {
		return itemSelecionado;
	}

	public void setItemSelecionado(PacienteDTO itemSelecionado) {
		this.itemSelecionado = itemSelecionado;
	}

	
}
