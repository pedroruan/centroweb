package br.com.centroteresadavila.beans;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import br.com.centroteresadavila.dto.PacienteDTO;
import br.com.centroteresadavila.service.PacienteService;


@Named(value="cadastroPacienteBean")
@RequestScoped
public class CadastroPacienteBean {

	private Logger LOGGER = Logger.getLogger(CadastroPacienteBean.class);
	
	@Inject
	private PacienteDTO pacienteDTO;
	
	@Inject
	private PacienteService pacienteService;
	
	public void cadastrar() {
		
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage message;
		
		if(pacienteService.cadastrar(pacienteDTO)) {
			message = new FacesMessage("Paciente Cadastrado com Sucesso");
			message.setSeverity(FacesMessage.SEVERITY_INFO);
			context.addMessage(null, message);
		}else {
			
			message = new FacesMessage("Falha ao realizar cadastro");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, message);
		}
		
        pacienteDTO = new PacienteDTO();
        
		this.pacienteDTO = null;
		
	}
	
	public void limpar() {
		pacienteDTO = new PacienteDTO();
		
	}


	public PacienteDTO getPacienteDTO() {
		return pacienteDTO;
	}


	public void setPacienteDTO(PacienteDTO pacienteDTO) {
		this.pacienteDTO = pacienteDTO;
	}

	
}
