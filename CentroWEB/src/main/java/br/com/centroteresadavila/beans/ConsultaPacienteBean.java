package br.com.centroteresadavila.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import br.com.centroteresadavila.dto.PacienteDTO;
import br.com.centroteresadavila.service.PacienteService;

@Named("consultaPacienteBean")
@javax.faces.view.ViewScoped
public class ConsultaPacienteBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Logger LOGGER = Logger.getLogger(ConsultaPacienteBean.class);
	
	
	@Inject transient
	private PacienteDTO pacienteDTO;
	
	private PacienteDTO itemSelecionado = new PacienteDTO();
	
	@Inject
	transient private PacienteService pacienteService;
	
	private List<PacienteDTO> listaPacientes = null;
	
	private Boolean renderizarTabela = Boolean.FALSE;
	
	public List<PacienteDTO> pesquisar() {
		listaPacientes = new ArrayList<PacienteDTO>();
		
		listaPacientes = pacienteService.pesquisar(pacienteDTO);
		
		if(listaPacientes.size()>0) {
			renderizarTabela = Boolean.TRUE;
		}
        
        return listaPacientes;
        
	}
	
	public void limpar() {
		pacienteDTO = new PacienteDTO();
		
	}
	
	public void prepararAlterar(PacienteDTO dto){

		this.itemSelecionado = dto;
		
		
	}
	
	public void alterar(){

		this.itemSelecionado = null;
		
		
	}
	
	public PacienteDTO getPacienteDTO() {
		return pacienteDTO;
	}


	public void setPacienteDTO(PacienteDTO pacienteDTO) {
		this.pacienteDTO = pacienteDTO;
	}

	public PacienteDTO getItemSelecionado() {
		return itemSelecionado;
	}

	public void setItemSelecionado(PacienteDTO itemSelecionado) {
		this.itemSelecionado = itemSelecionado;
	}

	public List<PacienteDTO> getListaPacientes() {
		return listaPacientes;
	}

	public void setListaPacientes(List<PacienteDTO> listaPacientes) {
		this.listaPacientes = listaPacientes;
	}

	public Boolean getRenderizarTabela() {
		return renderizarTabela;
	}

	public void setRenderizarTabela(Boolean renderizarTabela) {
		this.renderizarTabela = renderizarTabela;
	}

}
