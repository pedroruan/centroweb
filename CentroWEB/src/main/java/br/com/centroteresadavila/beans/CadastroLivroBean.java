package br.com.centroteresadavila.beans;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import br.com.centroteresadavila.dto.LivroDTO;
import br.com.centroteresadavila.service.LivroService;

@Named(value="cadastroLivroBean")
@RequestScoped
public class CadastroLivroBean {

	private Logger LOGGER = Logger.getLogger(CadastroLivroBean.class);
	
	@Inject
	private LivroDTO livroDTO;
	
	@Inject
	private LivroService livroService;
	
	public void cadastrar() {
		
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage message;
		
		if(livroService.cadastrar(livroDTO)) {
			message = new FacesMessage("Livro Cadastrado com Sucesso");
			message.setSeverity(FacesMessage.SEVERITY_INFO);
			context.addMessage(null, message);
		}else {
			
			message = new FacesMessage("Falha ao realizar cadastro");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, message);
		}
		
		livroDTO = new LivroDTO();
        
		this.livroDTO = null;
		
	}
	
	public void limpar() {
		livroDTO = new LivroDTO();
		
	}

	public LivroDTO getLivroDTO() {
		return livroDTO;
	}

	public void setLivroDTO(LivroDTO livroDTO) {
		this.livroDTO = livroDTO;
	}

}
