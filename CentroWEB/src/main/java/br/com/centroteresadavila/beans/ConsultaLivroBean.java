package br.com.centroteresadavila.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import br.com.centroteresadavila.dto.EmprestimoDTO;
import br.com.centroteresadavila.dto.LivroDTO;
import br.com.centroteresadavila.service.LivroService;

@Named("consultaLivroBean")
@javax.faces.view.ViewScoped
public class ConsultaLivroBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Logger LOGGER = Logger.getLogger(ConsultaLivroBean.class);
	
	
	@Inject transient
	private LivroDTO livroDTO;
	
	private LivroDTO itemSelecionado = new LivroDTO();
	
	@Inject
	transient private LivroService livroService;
	
	private List<LivroDTO> listaLivros = null;
	
	private Boolean renderizarTabela = Boolean.FALSE;
	
	public List<LivroDTO> pesquisar() {
		listaLivros = new ArrayList<LivroDTO>();
		
		listaLivros = livroService.pesquisar(livroDTO);
		
		if(listaLivros.size()>0) {
			renderizarTabela = Boolean.TRUE;
		}
        
        return listaLivros;
        
	}
	
	public void limpar() {
		livroDTO = new LivroDTO();
		
	}
	
	public void prepararAlterar(LivroDTO dto){

		LivroDTO retorno = new LivroDTO();
		
		retorno = livroService.detalharEmprestimo(dto);

		if(!Objects.isNull(retorno)) {
			dto.setEmprestimo(retorno.getEmprestimo());	
		}
		
		this.itemSelecionado = dto;
		
	}
	
	public void alterar(){

		this.itemSelecionado = null;
		
		
	}
	
	public LivroDTO getLivroDTO() {
		return livroDTO;
	}


	public void setLivroDTO(LivroDTO livroDTO) {
		this.livroDTO = livroDTO;
	}

	public LivroDTO getItemSelecionado() {
		return itemSelecionado;
	}

	public void setItemSelecionado(LivroDTO itemSelecionado) {
		this.itemSelecionado = itemSelecionado;
	}

	public List<LivroDTO> getListaLivros() {
		return listaLivros;
	}

	public void setListaLivros(List<LivroDTO> listaLivros) {
		this.listaLivros = listaLivros;
	}

	public Boolean getRenderizarTabela() {
		return renderizarTabela;
	}

	public void setRenderizarTabela(Boolean renderizarTabela) {
		this.renderizarTabela = renderizarTabela;
	}

}
